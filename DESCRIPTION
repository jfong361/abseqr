Package: abseqR
Type: Package
Title: High throughput antibody library quality control pipeline
Version: 0.99.0
Author: Jia Hong Fong
Maintainer: Jia Hong Fong <jfong361@gmail.com>
Description: Visualizes data generated from python package abseqPy.
License: MIT
Encoding: UTF-8
LazyData: true
Depends: R (>= 3.4)
Imports: 
    ggplot2,
    RColorBrewer,
    circlize,
    reshape2,
    VennDiagram,
    plyr,
    flexdashboard,
    BiocParallel (>= 1.1.25),
    png,
    grid,
    gridExtra,
    rmarkdown,
    knitr,
    vegan,
    ggcorrplot,
    ggdendro,
    plotly,
    BiocStyle
VignetteBuilder: knitr
RoxygenNote: 6.0.1
Collate: 
    'AbSeqCRep.R'
    'util.R'
    'distributions.R'
    'upstreamAnalysis.R'
    'productivityAnalysis.R'
    'primerAnalysis.R'
    'diversityAnalysis.R'
    'annotationAnalysis.R'
    'abundanceAnalysis.R'
    'plotter.R'
    'AbSeqRep.R'
    'abseqReport.R'
    'statistics.R'
    'pairwise.R'
SystemRequirements: pandoc
